# 例程说明
- 通过 [SPI sd卡模块](https://item.taobao.com/item.htm?id=642079682518) 从外挂的micro sd 卡(SPI)中加载游戏文件(Fat文件系统)
- [2.2寸ILI9341显示屏](https://item.taobao.com/item.htm?id=642079682518) (SPI接口240x320分辨率)显示游戏图象
- 支持开机动画、菜单选择游戏运行
- 支持音频输出(I2S)，使用 [CJMCUTRRS 3.5mm音频座模块](https://item.taobao.com/item.htm?id=642079682518) 输出音频更方便！
- 支持PCF8574 I/O拓展模块(I2C接口)控制游戏，拓展的8个I/O引脚用于游戏按键(上、下、左、右、A、B、select、start)
- 支持使用 [国内7针、9针NES游戏手柄](https://item.taobao.com/item.htm?id=642079682518) 控制游戏
- 支持 [国内MicroUSB接口的游戏手柄](https://item.taobao.com/item.htm?id=642079682518) 控制游戏


课程用到的开发板及硬件： https://item.taobao.com/item.htm?id=641542271685 淘口令：PxtZXYENAyt

项目使用 VScode、VScode插件cmake+c/c++开发，以上功能均可在 `CMakeLists.txt` 文件裁剪。

该项目同时移植到了 ESP32 上，视频教程：[ESP32|爷青回！ESP32 NES模拟器_NES游戏机掌机教程(开源+详细讲解实现代码！)](https://www.bilibili.com/video/BV1UB4y1P7iU?spm_id_from=333.788.b_636f6d6d656e74.4)


# 例程使用的开发板

[Raspberry pi pico](https://item.taobao.com/item.htm?id=641542271685)


## 开发板介绍

TODO

# 开发板及配套模块链接

[https://item.taobao.com/item.htm?id=642079682518](https://item.taobao.com/item.htm?id=642079682518)


# 视频教程

- 百问网官网: [https://www.100ask.net/](https://www.100ask.net/)
- Bilibili观看：[https://www.bilibili.com/video/BV1UB4y1P7iU](https://www.bilibili.com/video/BV1UB4y1P7iU)

